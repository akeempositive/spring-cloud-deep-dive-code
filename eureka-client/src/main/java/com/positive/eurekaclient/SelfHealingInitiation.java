package com.positive.eurekaclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.endpoint.event.RefreshEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
public class SelfHealingInitiation {


    @Autowired
    private ApplicationEventPublisher applicationContextPublisher;

    public boolean commenceSelfHealing(SelfHealingStrategy strategy){
        strategy.heal();
        applicationContextPublisher.publishEvent(new RefreshEvent(this, "RefreshEvent", "Refreshing scope"));
        return true;
    }
}
