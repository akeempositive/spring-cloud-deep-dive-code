package com.positive.eurekaclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@Controller
@RequestMapping("client")
public class EurekaClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaClientApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Autowired
    private Environment environment;

    @GetMapping("hello")
    public @ResponseBody String getGreeting(){
        return String.format("<h1>Hello, from application instance on port: %s</h1>", environment.getProperty("local.server.port"));
    }

    @GetMapping("dollar-to-naira")
    public @ResponseBody String getDollarToNairaValue(){
        return ""+ (375 + (int )(Math.random() * (105)));
    }

}
