package com.positive.eurekaclient;


public interface SelfHealingStrategy {

    void heal();
}
