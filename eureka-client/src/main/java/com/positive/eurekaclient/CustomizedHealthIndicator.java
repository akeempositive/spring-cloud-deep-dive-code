package com.positive.eurekaclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomizedHealthIndicator implements HealthIndicator, SelfHealingStrategy {

    @Autowired
    private SelfHealingInitiation selfHealingInitiation;

    int count =0;

    @Override
    public Health health() {
         if(isHealthy()){
             return Health.up().withDetail("STATUS", "Available").build();
         }else {
             count = 0;
             selfHealingInitiation.commenceSelfHealing(this);
             return Health.down().withDetail("STATUS","Not Available").build();
         }

    }

    private boolean isHealthy(){
        count++;
        return  count % 5 != 0;
    }

    @Override
    public void heal() {
        // properly reset some environmental variable or change system permission.
        // It should be specific to the health indicator.
        // It should be sufficient to make this indicator become healthy again
    }
}
