package com.positive.eurekaclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("eureka-client")
public interface EurekaClientFeign {

    @GetMapping("client/hello")
    String getGreeting();

    @GetMapping("client/dollar-to-naira")
    String dollarToNairaValue();
}
