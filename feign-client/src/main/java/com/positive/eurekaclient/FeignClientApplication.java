package com.positive.eurekaclient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@Controller
@RequestMapping("feign")
@EnableCircuitBreaker
public class FeignClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(FeignClientApplication.class, args);
    }

    private String lastValue = "375";

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Autowired
    private EurekaClientFeign eurekaClientFeign;

    @GetMapping("hello")
    public @ResponseBody String getGreeting(){
        return eurekaClientFeign.getGreeting();
    }

    @GetMapping("dollar-to-naira")
    @HystrixCommand(fallbackMethod = "faultTolerant")
    public @ResponseBody String dollarToNaira(){
        String value =  eurekaClientFeign.dollarToNairaValue();
        lastValue = value;
        return "<h1>Value from cloud distributed service for USD-NGN is " + value +"</h1>";
    }

    public @ResponseBody  String faultTolerant(){
        return String.format("<h1>Cloud service not available.<br/>Fault Tolerancy<br/>Last saved value for USD-NGN is %s</h1>", lastValue );
    }

}
